#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.

import re
import hashlib

class ordered_dict(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self._order = self.keys()

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        if key not in self._order:
            self._order.append(key)

    def __delitem__(self, key):
        dict.__delitem__(self, key)
        self._order.remove(key)

    def order(self):
        return self._order[:]

    def items(self):
        return [(key,self[key]) for key in self._order]
        
def join_headers(d):
	hd = ''
	for key_val in d.items():
		hd += "%s: %s\r\n" % key_val
	hd += "\r\n"
	return hd
	
def parse_headers(header):
	od = ordered_dict()
	for a in re.findall(r"(?P<name>.*?): (?P<value>.*?)\r\n", header):
		od[a[0]] = a[1]
	return od

def get_hash(txt):
	a = hashlib.md5()
	a.update(txt)
	return a.hexdigest()
	
def text_2_byte_range(txt):
	rslt = re.compile("(.*)-(.*)").search(txt)
	a = int(rslt.group(1).strip())
	b = rslt.group(2).strip()
	if not b:
		b = db.MAX_LEN#as large as possible
	else:
		b = int(b)
	return [a,b]
