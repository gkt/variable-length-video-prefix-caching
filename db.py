#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.

import os
import sqlite3
import logging
import re
import sys

import popularity
import config
from common import *

LOG_FILE = os.path.expanduser('~') + "/" + config.PROG_DIR_NAME + "/" + config.LOG_NAME #created by logger

schema = """create table cache_info (
								ID      	  	integer primary key,
								url				text,
								last_popularity	REAL,
								url_hash		text,
								byte_range		text,
								file_loc		text,
								total_size		INTEGER,
								header			text
								);
	create table request (
								ID        		integer primary key,
								client_ip		text,
								date 			DATETIME,
								url_hash		text,
								byte_range_used	text
								);
	create table stats (
								ID        		integer primary key,
								client_ip		text,
								arrival_date 			DATETIME,
								url				text,
								req_ran_fr		INTEGER,
								req_ran_to		INTEGER,
								bytes_2_cl		INTEGER,
								cac_ran_fr  	INTEGER,
								cac_ran_to 		INTEGER,
								pop				text,
								arrival_stamp	text,
								dispatch_stamp	text
								);
	create table more_space_runs (
								ID        		INTEGER primary key,
								start_time 		DATETIME,
								end_time		DATETIME,
								succeeded 		INTEGER,
								hit_count_start INTEGER,
								hit_count_end 	INTEGER
								);
	create table videos_changed (
								ID        		INTEGER primary key,
								run_id 			INTEGER,
								url_hash 		text,
								previous_byte_range text,
								new_byte_range	text,
								start_time 		DATETIME,
								end_time 		DATETIME
								);"""

dblogger = logging.getLogger('main.db_')

class db():
	""""""

	def __init__(self):
		self.logger = logging.getLogger('main.db')
		self.logger.info("Instantiating db class")
		self.homedir = os.path.expanduser('~')
		self.progdir = self.homedir + "/" + config.PROG_DIR_NAME
		self.cachedir = self.progdir + "/" + config.CACHE_DIR_NAME
		self.db_file = self.progdir + "/" + config.TABLE_NAME
		
		self.check_files()
		self.connect()
		
		self.pop = popularity.popularity(self.conn)

	def check_files(self):
		if not os.path.exists(self.progdir):
			self.logger.info("Creating " + self.progdir)
			os.makedirs(self.progdir)
			os.makedirs(self.cachedir)
		elif not os.path.exists(self.cachedir):
			self.logger.info("Creating " + self.cachedir)
			os.makedirs(self.cachedir)
		
	def connect(self):
		file_exists = os.path.exists(self.db_file)
		self.conn = sqlite3.connect(self.db_file,check_same_thread = False,
					isolation_level = None) # autocommit mode with threading
		if not file_exists:
			self.logger.info("Creating " + self.db_file)
			self.conn.executescript(schema)
					
	def insert(self,lst):
		self.conn.execute("""insert into cache_info (url,last_popularity,url_hash, byte_range, file_loc, total_size,header) values (?,?,?,?,?,?,?)""",lst)

	def extract(self,query):
		cursor = self.conn.cursor()
		return cursor.execute(query).fetchall()
				
	def is_vid(self,url):
		return re.compile(".mp4").search(url) and not re.compile("google").search(url) # return none on mismatch
		
	def update_stats(self,stats):
		self.logger.info("Updating stats with %s" % stats)
		self.conn.execute("""insert into stats (client_ip, arrival_date, url,
			 req_ran_fr, req_ran_to, bytes_2_cl, cac_ran_fr, cac_ran_to, pop,
			 arrival_stamp, dispatch_stamp)
			 values (?,?,?,?,?,?,?,?,?,?,?)""",stats)
		
	def read_popularity(self,hsh,byte_range,f_bytes,total_vid_size,client_address,stats):
		ret = self.pop.popularity_check(client_address,hsh,byte_range,f_bytes,total_vid_size)#byte length and popularity
		pop_byte_len = ret[0]
		stats[8] = pop_byte_len
		self.update_stats(stats)
		
		ret2 = self.check_popularity_returned_length(pop_byte_len,f_bytes,byte_range)
		if ret2:
			return ret2 + [ret[1]] #add popularity to list
	
	#passed variables are changed here
	def check_popularity_returned_length(self,pop_byte_len,f_bytes,byte_range):
		if pop_byte_len > len(f_bytes):
			self.logger.info("""Popularity check: Returned byte length(%d) is more then fetched bytes, continuing with request range(%s)""" % (pop_byte_len,byte_range))
			return [f_bytes,byte_range]
		

		new_range_end = int(pop_byte_len - (byte_range[0] + 1))# range starts from 0
		if new_range_end < byte_range[0]:#TODO should cached video be reduced here
			self.logger.info("""Popularity check: Skipping cache, byte range in update request(%s-%s), prefix len from popularity(%d)""" % (byte_range[0],byte_range[1],pop_byte_len))
			return []
		else:
			f_bytes = f_bytes[:new_range_end + 1]
			self.logger.info("""Popularity check: request range changed from %s-%s to %s-%d with %d bytes""" % (byte_range[0],byte_range[1],byte_range[0],new_range_end,len(f_bytes)))
			byte_range[1] = new_range_end
			return [f_bytes,byte_range]

				
	def get_cache(self,url,rng):# should also be locked upto the file read line 
		tmp = self.extract("""select file_loc,byte_range,header,total_size
				 from cache_info where url_hash = '%s'""" % get_hash(url))
		if tmp:# assuming that if entry exist in cache_info then it will contain some non-zero prefix data
			tmp = tmp[0]
			self.logger.info("HIT on %s" % url)			
			c_rng = text_2_byte_range(tmp[1])

			#check if any part of requested range can be provided by cached range
			if self.in_range_chk(rng,c_rng):
				# plist is list of range tupples, plist[0]  be cached part and plist[1] is to be requested from remote server
				plist = self.get_list(tmp[1],rng,url)
				
				fp = open(tmp[0],"rb")
				in_cache_range = plist[0]
				#Its already verified that rng is contained in c_rng				
				#print len(fp.read())
				fp.seek(0)
				fp.seek(in_cache_range[0])
				c_bytes = fp.read(in_cache_range[1] - in_cache_range[0] + 1)
				if len(c_bytes) != in_cache_range[1] - in_cache_range[0] + 1:
					self.logger.error("Cache fetch error: length mismatch")
					print tmp
					sys.exit(0)
				self.logger.info("Fetched %d bytes from cache" % len(c_bytes))
			
				headers = tmp[2]
				return plist,c_bytes,headers,tmp[3]
			else:
				self.logger.info("Requested range(%s) not in cache(%s)" % (rng,tmp[1]))
				plist = self.get_list(None,rng,url)
				return plist,'','',''
		else:
			self.logger.info("MISS on %s" % url)
			plist = self.get_list(None,rng,url)
			return plist,'','',''
		
	def update_cache(self,byte_range,url,f_bytes,header,total_vid_size,client_address,stats):
#check passes arguments
		rl = byte_range[1] - byte_range[0] + 1
		if rl > len(f_bytes):
			self.logger.warning("In update request byte size(%d) is less then byte range(%d-%d) length" % (len(f_bytes),byte_range[0],byte_range[1]))
			if len(f_bytes) != 0:
				byte_range[1] = byte_range[0] + len(f_bytes) - 1 # range starts from 0 TODO CRITICAL think of alternative(other places also)
			else:
				byte_range[1] = 0
			self.logger.info("Byte range in request changed to %d-%d" % (byte_range[0],byte_range[1]))
			
		elif rl < len(f_bytes):
			self.logger.error("Corrupted bytes received")
			sys.exit(0)
					
#query database
		hsh = get_hash(url)
		qry = self.extract("""select file_loc,byte_range,total_size
					 from cache_info where url_hash = '%s'""" % hsh)

#update stats list
		stats[0] = client_address
		stats[2] = url
		stats[3] = byte_range[0]
		stats[4] = byte_range[1]
		stats[5] = len(f_bytes)
		
		if(qry):
			tmp = text_2_byte_range(qry[0][1])
			stats[6] = tmp[0]
			stats[7] = tmp[1]
		else:
			stats[6] = 0
			stats[7] = 0
		
#MODIFY byte_range AND f_bytes USING popularity
		#change byte_range and f_bytes according to popularity (TODO pass by reference doesn't work for f_bytes ??)
		popularity = 0
		ret = self.read_popularity(hsh,byte_range,f_bytes,total_vid_size,client_address,stats)
		if ret:
			(f_bytes,byte_range,popularity) = ret
		else:
			return		
		
		cache_size = self.get_cache_size(hsh)
		self.logger.info("Cache size:%d Bytes" % cache_size)# TODO error while converting to MB ??
		# do until enough space is created
		while cache_size + len(f_bytes) > config.MAX_CACHE_SIZE:
			ret = self.pop.create_more_space()
			if not ret:
				#logged at popularity.py
				
				# space can't be created to fit whole prefix len returned by pop
				# so lets fit as much as we can in available free space
				cache_size = self.get_cache_size(hsh)
				avail_free_space = config.MAX_CACHE_SIZE - cache_size
				
				if avail_free_space > 0:
					# adjust f_bytes and byte_range according to avail_free_space
					self.logger.info("Modifying f_bytes and byte_range according to avail_free_space")
					self.check_popularity_returned_length(avail_free_space,f_bytes,byte_range)
				else:
					self.logger.warning("No more free space left on cache, data won't be cached")
				
				return
			[pop_byte_len,_] = self.pop.get_popularity_based_prefix_length(hsh,total_vid_size)# These lines are needed only if db.py is accessed
			ret = self.check_popularity_returned_length(pop_byte_len,f_bytes,byte_range)  #  in || (even if they are read only)
			if ret:
				(f_bytes,byte_range) = ret
			else:
				return
			
			cache_size = self.get_cache_size(hsh)

		
#UPDATE DB WITH byte_range and f_bytes
		# first time url
		if not qry:
			if byte_range[0] != 0:
				self.logger.info("Skipping caching: Non prefix bytes")
				return
			
			file_loc = self.cachedir + '/' + hsh
					
			fp = open(file_loc,"wb")
			fp.write(f_bytes)
		
			lst = ([url,
					popularity,
					hsh,
					"%d-%d" % (byte_range[0],byte_range[1]),
					file_loc,
					total_vid_size,
					header
					])
			self.insert(lst)
			#print lst
			self.logger.info("Cache appended with byte range %d-%d (%d bytes)" % (byte_range[0],byte_range[1],len(f_bytes)))
		# url already in database
		else:
			qry = qry[0]
			c_rng = text_2_byte_range(qry[1])
			
			if c_rng[1] >= byte_range[1]:
				self.logger.error("cache data is more then update request: cache(%s),request(%s)" % (qry[1],byte_range))
			elif c_rng[1] + 1 >= byte_range[0]:
			
				file_loc = qry[0]					
				fp = open(file_loc,"r+b")
				buf = fp.read()
				buf1 = buf[:byte_range[0]]# not including byte_range[0]
				
				#clear file's content
				fp.seek(0)
				fp.truncate()
							
				fp.write(buf1 + f_bytes)
				self.conn.execute("""update cache_info set byte_range = '0-%s'
						,last_popularity = %d where url_hash='%s'""" % (byte_range[1],popularity,hsh))
				self.logger.info("Cache updated: %s to %d-%d (%d bytes)" % (c_rng,byte_range[0],byte_range[1],len(buf1 + f_bytes)))
				#print """update cache_info set byte_range = '0-%s'
				#		 where url_hash='%s'""" % (byte_range[1],hsh)
			else:
				self.logger.error("Non contiguous/overlapping bytes ranges: cache(%s),request(%s)" % (qry[1],byte_range))
	
	def get_cache_size(self,hsh):
		qry = self.extract("select url,byte_range from cache_info")
		total_size = 0
		if not qry:
			return total_size
		else:
			#qry = qry[0]
			for a in qry:
				if get_hash(a[0]) != hsh:
					lst = text_2_byte_range(a[1])
					total_size += int(lst[1] - lst[0] + 1)
			return total_size
	
	def get_list(self,tmp,requested_range,url):
		if not tmp:# if no cache available
			return [[requested_range[0],requested_range[1],False,'']]
			
		url_hash = get_hash(url)
		cache_range = text_2_byte_range(tmp)
		process_list = []
		if requested_range[1] > cache_range[1]:
			if requested_range[0] > cache_range[1]:
				process_list.append([requested_range[0],requested_range[1],False,''])
			else:
				process_list.append([requested_range[0],cache_range[1],True,url_hash])
				process_list.append([cache_range[1] + 1, requested_range[1], False, ''])
			
		else:
			process_list = [[requested_range[0],requested_range[1],True,url_hash]]
			
		return process_list	
		
	def in_range_chk(self,a,b):
		#assuming b will always be prefix starting form 0
		return a[0] <= b[1] and a[0] >= b[0]

	
	
