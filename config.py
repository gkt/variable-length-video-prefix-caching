#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.

MAX_LEN = 1111111111 # ~1 GB
MIN_BYTE_2_CLIENT = 1000
MAX_CACHE_SIZE = 1024*1024*100 #bytes

PROG_DIR_NAME = 'VPC_Proxy'
CACHE_DIR_NAME = 'cache'
LOG_NAME = "log.out"
TABLE_NAME = "table.db"
