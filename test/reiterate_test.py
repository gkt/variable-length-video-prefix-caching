#!/usr/bin/env python

#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.

#re-request all the videos using stats table

__version__ = '0.3'

import socket
import select
import sys
import threading
import re
import random
import os
import common
import sqlite3
import test
import datetime
import time

PROXY_IP = '0.0.0.0'
PROXY_PORT = 7070

CONNECT_MSG = """accept-language: en-US,en;q=0.8\r
connection: close\r
range: bytes=0-50\r
accept: */*\r
user-agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7\r
accept-charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3\r
host: pyrova.com\r
referer: http://pyrova.com/sniffer/1.html\r
accept-encoding: identity;q=1, *;q=0\r\n\r\n"""

class connect(threading.Thread):
	def __init__(self,msg):
		threading.Thread.__init__(self)
		self.msg = msg
	def run(self):
		# Create a TCP/IP socket
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		# Connect the socket to the port where the server is listening
		server_address = (PROXY_IP, 7070)
		print 'connecting to %s port %s' % server_address
		sock.connect(server_address)
		print "Sending CONNECT msg"		
		sock.sendall(self.msg)
		return self.read(sock,20000)
				
	def read(self,sock,wait_count):
		buf = ""
		i = 0
		while True:
			i += 1
			(ins, _, exs) = select.select([sock], [], [sock], 3)
			if ins:
				 buf += sock.recv(1000)
			if i > wait_count:
				return buf

def request_video(seq):
	homedir = os.path.expanduser('~')
	PROG_DIR_NAME = 'pytrace_proxy'
	progdir = homedir + "/" + PROG_DIR_NAME
	db_file = progdir + '/table.db'
	SERVER_IP = 'pyrova.com' 

	file_exists = os.path.exists(db_file)
	conn = sqlite3.connect(db_file,detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)

	if not file_exists:
		exit(0)

	last_date = conn.execute('select min(date) as "[timestamp]" from stats order by date').fetchall()[0][0]

	dump =	conn.execute('select url,req_ran_fr,req_ran_to,date as "[timestamp]" from stats order by date')
	for x in dump:
		td = (x[3]-last_date)
		last_date = x[3]
		value = float(td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
		print "Sleeping for %f seconds" % value 
		time.sleep(value)
		print "Requesting ",x[0]," from: ",x[1]," to:",x[2]
		t_hd = common.parse_headers(CONNECT_MSG)
		t_hd['mypath'] = x[0]
		t_hd['Range'] = 'bytes=%d-%d' % (x[1],x[2])
		msg = 'CONNECT ' + SERVER_IP + ':80 HTTP/1.0\r\n' + common.join_headers(t_hd)

#		if seq:
#			print "FF"
#			connect(msg).run()
#		else:
#			connect(msg).start()

if __name__ == "__main__":
	request_video(True)
