#!/usr/bin/env python

#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.

__version__ = '0.5'

import socket
import select
import sys
import threading
import time
import re
import random
import urllib2

sys.path.append('..')
import common


PROXY_IP = 'localhost'
PROXY_PORT = '8000'
PROXY_ADD = '%s:%s'%(PROXY_IP,PROXY_PORT)

SERVER_IP = 'localhost'
SERVER_PORT = '80'
SERVER_ADD = '%s:%s'%(SERVER_IP,SERVER_PORT)


MAX_THREAD_COUNT = 50
JUMP_PROB = 30 # out of 100
MAX_REQ_SIZE = 10*1024*1024

CONNECT_MSG = """accept-language: en-US,en;q=0.8\r
connection: close\r
range: bytes=0-50\r
accept: */*\r
user-agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7\r
accept-charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3\r
host: pyrova.com\r
referer: http://pyrova.com/sniffer/1.html\r
accept-encoding: identity;q=1, *;q=0\r\n\r\n"""

urls = {}
URL_FORMAT = 'http://' + SERVER_IP + '/%s'

def populate_urls():
	proxy = urllib2.ProxyHandler({'http': PROXY_ADD})
	opener = urllib2.build_opener(proxy)
	urllib2.install_opener(opener)
	response = urllib2.urlopen('http://' + SERVER_IP + '/VPC_test_videos/urls')
	txt = response.read()
	cnt = 1
	for a in txt.split('\n'):
		if a:
			b = a.split('$')
			urls[cnt] = [URL_FORMAT % b[0], int(b[1]), int(b[2])]
			cnt += 1
	

class Thread_Pool():
	def __init__(self,max_threads):
		self.thread_count = 0
		self.max_threads = max_threads
		self.msg_list_Lock = threading.Condition(threading.Lock())
		self.msg_count_Lock = threading.Condition(threading.Lock())
		self.msg_list = []
		self.done = False
		self.msg_recv = 0
		self.msg_sent = 0
		self.start()
		
	def end(self):
		self.done = True
	def inc_msg_recv(self):
		self.msg_count_Lock.acquire()
		try:
			self.msg_recv += 1
		finally:
			self.msg_count_Lock.release()		
	def inc_msg_sent(self):
		self.msg_count_Lock.acquire()
		try:
			self.msg_sent += 1
		finally:
			self.msg_count_Lock.release()
		
	def queue_task(self,msg):
		self.msg_list_Lock.acquire()
		try:
			self.inc_msg_sent()
			self.msg_list.append(msg)
		finally:
			self.msg_list_Lock.release()

		
	def start(self):
		for a in range(self.max_threads):
			self.thread_count += 1
			connect(self.thread_count,self).start()
			
	def get_new_msg(self):
		self.msg_list_Lock.acquire()
		#mem_profiler()# here access will be serial
		try:
			if self.done and len(self.msg_list) == 0:
				return None
			elif len(self.msg_list) != 0:
				return self.msg_list.pop()
			else:
				return 'wait'
		finally:
			self.msg_list_Lock.release()
	def __del__(self):
		print 'Total msg pooled: ',self.msg_sent
		print 'Total msg sent and received successfully',self.msg_recv
		

class connect(threading.Thread):
	def __init__(self,i,pool):
		self.pool = pool
		threading.Thread.__init__(self)
		self.i = i
		print self.i,' starting.'

	def send(self,msg):
		# Create a TCP/IP socket
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.settimeout(100)# blocking
		# Connect the socket to the port where the server is listening
		server_address = (PROXY_IP, int(PROXY_PORT))
		print '%d connecting to %s' % (self.i,server_address)
		sock.connect(server_address)
		#print "Sending CONNECT msg"
		sock.sendall(msg)
		buf = self.read(sock,200)
		sock.close()
		rslt = re.compile("\r\n\r\n").search(buf)
		headers = buf[:rslt.start() + 2]
		data = buf[rslt.end():]
		self.pool.inc_msg_recv()
		print '%d received %d bytes' % (self.i,len(data))
	
	def run(self):
		while True:
			msg = self.pool.get_new_msg()
			if msg == 'wait':
				print self.i," waiting"
				time.sleep(1)
			elif msg == None:
				print self.i,' closing.'
				return
			else:
				self.send(msg)

	def read(self,sock,wait_count):
		buf = ""
		count = 0
		while True:
			count += 1
			(ins, _, exs) = select.select([sock], [], [sock], 3)
			if ins:
				tmp = sock.recv(1024*10)
				if tmp:# remove this to simulate early connection termination
					count = 0	
				buf += tmp
			if count > wait_count:
				return buf
	
def create_msg(path,byte_range):
	t_hd = common.parse_headers(CONNECT_MSG)
	t_hd['mypath'] = path
	t_hd['range'] = 'bytes=%d-%d' % (byte_range[0],byte_range[1])
	return 'CONNECT ' + SERVER_IP + ':80 HTTP/1.0\r\n' + common.join_headers(t_hd)
	
def random_range(count,seq,range_len = None):
	t_hd = common.parse_headers(CONNECT_MSG)
	if seq:
		pool = Thread_Pool(1)
	else:			
		pool = Thread_Pool(MAX_THREAD_COUNT)

	for i in range(count):
		ind = random.randint(1,len(urls))
		random.seed()
		if not range_len:
			range_len = random.randint(1,int(urls[ind][1]))
			random.seed()
		start = random.randint(0,urls[ind][1] - 1 - range_len)
		msg = create_msg(urls[ind][0],[start,range_len + start - 1])
		pool.queue_task(msg)	
	
	pool.end()

def random_range_prefix(count,seq,range_len = None):
	rand_flag = False
	if range_len == None:
		rand_flag = True
	if seq:
		pool = Thread_Pool(1)
	else:			
		pool = Thread_Pool(MAX_THREAD_COUNT)

	for i in range(count):
		ind = random.randint(1,len(urls))
		random.seed()
		if rand_flag:
			range_len = random.randint(1,int(urls[ind][1]))
			random.seed()
		start = 0
		msg = create_msg(urls[ind][0],[start,range_len + start - 1])
		pool.queue_task(msg)	
	
	pool.end()

def random_range_prefix_prob(count,seq):
	random.seed()
	if seq:
		pool = Thread_Pool(1)
	else:
		pool = Thread_Pool(MAX_THREAD_COUNT)

	for i in range(count):		
		ind = random.randint(1,len(urls))
		range_len = random.randint(1,int(urls[ind][1]))
		start = 0
		msg = create_msg(urls[ind][0],[start,range_len + start - 1])
		pool.queue_task(msg)
		
		prob = random.randint(1,100)
		if prob <= JUMP_PROB:
			start = random.randint(1,int(urls[ind][1]) - 1)
			end = random.randint(start + 1,int(urls[ind][1]))
			msg2 = create_msg(urls[ind][0],[start,end])
			pool.queue_task(msg2)	
	pool.end()
	
def viral_simulation(count, seq, vid_num, prob):
	random.seed()
	if seq:
		pool = Thread_Pool(1)
	else:
		pool = Thread_Pool(MAX_THREAD_COUNT)
		
	for i in range(count):
		p = random.randint(1,100)
		if p <= prob:
			ind = random.randint(1,vid_num)
		else:
			ind = random.randint(vid_num,len(urls))
		range_len = random.randint(1,int(urls[ind][1]))
		start = 0
		msg = create_msg(urls[ind][0],[start,range_len + start - 1])
		pool.queue_task(msg)
	
		prob = random.randint(1,100)
		if prob <= JUMP_PROB:
			start = random.randint(1,int(urls[ind][1]) - 1)
			end = random.randint(start + 1,int(urls[ind][1]))
			msg2 = create_msg(urls[ind][0],[start,end])
			pool.queue_task(msg2)	
	pool.end()
		
#def only_one_extra_prefix(count,seq,range_len = None):
#	choosen_one = 5
#	extra_count = 2*count 
#	mini = 4*1024*1024
#	if seq:
#		pool = Thread_Pool(1)
#	else:			
#		pool = Thread_Pool(MAX_THREAD_COUNT)
#	for i in range(count):
#		for ind in urls:
#			print urls[ind][0] 
#			random.seed()
#			if not range_len:
#				range_len = mini + random.randint(0,int(urls[ind][1]) - mini)
#				random.seed()
#			start = 0
#			msg = create_msg(urls[ind][0],[start,range_len + start - 1])
#			pool.queue_task(msg)	

#	only_one_prefix(choosen_one,extra_count,True)
#	pool.end()

#request on one video choosen one count number of times
def only_one_prefix(ind,count,range_len = None):
	rand_flag = False
	if range_len == None:
		rand_flag = True
	mini = 1024
	random.seed()
	msg_lst = []
	for i in range(count):		
		if rand_flag:
			range_len = mini + random.randint(0,int(urls[ind][1]) - mini)
		start = 0
		while True:# break request acc to MAX_REQ_SIZE
			if range_len > MAX_REQ_SIZE:				
				msg = create_msg(urls[ind][0],[start,MAX_REQ_SIZE + start - 1])
				start += MAX_REQ_SIZE
				range_len -= MAX_REQ_SIZE
				msg_lst.append(msg)
			else:
				msg = create_msg(urls[ind][0],[start,range_len + start - 1])
				msg_lst.append(msg)
				break			
	return msg_lst

def test_with_data():
	print "Creating msg list.."
	msg_lst = []
	lst = [[a,int(v[2])] for a,v in urls.items()]
	lst2 = []
	for i in lst:
		lst2.extend([i[0]]*i[1])
	del lst
	while len(lst2) > 0:
		random.seed()
		ind = random.randint(0,len(lst2) - 1)
		for a in only_one_prefix(lst2[ind],1):
			msg_lst.append(a)
		del lst2[ind]
	
	return msg_lst
	
def run(msg_lst,seq):
	print "Total runs: %d"%(len(msg_lst))
	if seq:
		pool = Thread_Pool(1)
	else:			
		pool = Thread_Pool(MAX_THREAD_COUNT)
		
	for a in msg_lst:
		pool.queue_task(a)		
	pool.end()
	
if __name__ == "__main__":
	populate_urls()
	run(test_with_data(),seq = False)
	#only_one_prefix(1,1,True)
	#only_one_extra_prefix(1000,1)
	#random_range_prefix_prob(100,False)
	#random_range_prefix_prob(100,False)
	#viral_simulation(300, False, 10, 85)
