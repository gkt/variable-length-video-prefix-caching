#!/usr/bin/env python

#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.





#Instructions:
#
#For simple test:
#1.Run with sudo permission.
#2.This file accept three command line arguments <arg1> <arg2> <arg3>
#<arg1> is the no of video files to create.
#<arg2> is the lower bound on each video size in MB.
#<arg3> is the upper bound on each video size in MB.
#
#For test with data:
#1.Run with sudo permission.
#2.This file accept three command line arguments -d <arg1> <arg2>
#<arg1> is the name of data file.
#<arg2> is the maximum space to allocate for test videos in MB.


__version__ = '0.5'

import os
import sys
import random
import getopt

TEST_DIR = '/opt/lampp/htdocs' + '/VPC_test_videos'
CATLOGUE_FILE = 'urls'
CATLOGUE_FORMAT = 'VPC_test_videos/%d.mp4$%d$%d\n'
CHAR = '0'

BIT_RATE = 300*1024 # for 360p	300 kbit/s

def generate_video_parameters():
	print "Generating video parameters.."
	start = 1
	stop = int(sys.argv[1]) + 1
	min_len = int(sys.argv[2])*1024*1024
	max_len = int(sys.argv[3])*1024*1024
	video_para = []
	for i in xrange(start,stop,1):
		length = random.randint(min_len,max_len)
		video_para.append([i,length,0])
	return video_para

def create_videos(video_para):
	for a in video_para:
		f_name = (TEST_DIR + '/%d.mp4')% a[0]
		print "Creating " + f_name , a[1], a[2]
		fp = open(f_name,'w')
		fp.write(CHAR*int(a[1]))
		fp.close()
		
def catalogue(video_para):
	cf = TEST_DIR + '/' + CATLOGUE_FILE
	print "Creating catalogue file " + cf
	fp = open(cf, 'w')
	for i in video_para:
		fp.write(CATLOGUE_FORMAT % (i[0],i[1],i[2]))
		
def cleaner():
	print "Removing existing test videos.."
	for f in os.listdir(TEST_DIR):
		os.remove(TEST_DIR + '/' + f)
	
def simple_test():
	#generate test videos parameters
	video_para = generate_video_parameters()
	
	#create test videos
	create_videos(video_para)
	
	#create url file
	catalogue(video_para)
		
def test_with_data(fname,args,p):
	#generate test videos parameters	
	pdata = []
	try:
		print "Reading and sorting data file.."
		fp = open(p + "pdata.txt","r")
		for a in fp.readlines():
			tmp = a.split("|")
			pdata.append([int(tmp[0]),int(tmp[1])])
		fp.close()
	except IOError:
		print "Reading and sorting BIG data file.." 
		fp = open(p + fname,"r")
		for a in fp.readlines():
			tmp = a.split('|')
			if tmp[2] != '0':
				tmp2 = tmp[1].split(':')
				tmp[1] = int(tmp2[1]) + int(tmp2[0])*60
				pdata.append([tmp[1],int(tmp[2])])
		pdata.sort(key = lambda k: k[1], reverse = True)
		fp.close()
		fp = open(p + "pdata.txt","w")
		for a in pdata:
			if a[1]:
				fp.write("%s|%s\n" % (a[0],a[1]))
		fp.close()
	
	print "Selecting test videos.."
	total_size = 0
	max_size = int(args[0])*1024*1024
	video_para = []
	cnt = 1
	f_cnt = 0
	m_f_cnt = 20
	runs = 0
	while True:
		ind = random.randint(0,len(pdata))
		size = pdata[ind][0]*BIT_RATE
		if total_size + size <= max_size:
			pop = pdata[ind][1]
			video_para.append([cnt,size,pop])
			del pdata[ind]
			cnt += 1
			total_size += size
			runs += pop
			
		else:
			f_cnt += 1
			if f_cnt > m_f_cnt:
				break
	
	#create test videos
	create_videos(video_para)
	
	#create url file
	catalogue(video_para)
	print "Total runs: %d"% (runs)
	
if __name__ == "__main__":
	if os.path.exists(TEST_DIR):
		cleaner()
	else:
		print "Creating:" + TEST_DIR
		os.makedirs(TEST_DIR)
	
	try:
		optlist, args = getopt.getopt(sys.argv[1:], 'd:')
	except getopt.GetoptError, err:
		print "Read Instructions."
		
	if optlist:
		for o,a in optlist:
			if o == '-d':
				test_with_data(a,args,"../extra/test_data_gen/")
			else:
				print "Read Instructions."
	else:
		simple_test()
