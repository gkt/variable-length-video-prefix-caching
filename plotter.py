#! /bin/python

#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3
import sys
import os
from numpy import *
import Gnuplot, Gnuplot.funcutils
import subprocess
import time
import re

import common
import config


ERROR = 0
OUT_CNT = 0


TABLE_FILE = os.path.expanduser('~') + "/" + config.PROG_DIR_NAME + "/" + config.TABLE_NAME
conn = sqlite3.connect(TABLE_FILE)
cursor = conn.cursor()
qry = "select * from stats"
rslt = cursor.execute(qry).fetchall()

def chk_x(time):
	global ERROR
	if time < 0:# to handle special extreme cases, usually occurs due to invalid data entry in db
		ERROR += 1
		return False
	else: return True
	
	
def get_bytes_from_remote(a,b,c):
	if a > c:
		return b - a
	elif b < c:
		return 0
	else:
		return (b - a) - (c - a)
		
		
def get_bytes_from_cache(a,b,c):
	if a > c:
		return 0
	elif b < c:
		return b - a
	else:
		return (c - a)
	
	
def plotter(points,xl = '',yl = ''):
	g = Gnuplot.Gnuplot(debug=1,persist = 1)
	#g('set terminal wxt')
	#g("set multiplot")
	g.title('Bandwidth')
	g.xlabel('time ' + xl)
	g.ylabel('data ' + yl)
	g('set autoscale x')
	g('set grid')
	#g('set term x11 size 1000 2000')
	g('set style data impulses') #points, lines, linespoints, steps, boxes, errorbars, impulses
	#g('set pointsize 5')
	g.plot(points)
	s = raw_input("Enter 'y' to save graph:")
	if s == 'y':
		fname = raw_input('File name:')
		fname += '.ps'
		g.hardcopy(fname, enhanced=1, color=1)
		print 'Saved plot to postscript file ' + fname
		
def multiplotter(points, graphs = None, title = '', xl = '', yl = ''):# anti-aliasing depends on the renderer
	global OUT_CNT
	OUT_CNT += 1
	f = open('data', 'w')
	for a in points:
		f.write(str(a[0]) + ' ')
		for i in a[1]:
			f.write(str(i) + ' ')
		f.write('\n')
	f.close()
	arg = ''
	arg += "set terminal postscript enhanced color solid size 20,20;" #eps monocrome enhanced dashed;
#	arg += "set terminal wxt;"
	arg += "set output 'out" + str(OUT_CNT) + ".ps';"
	arg += "set style data lines;"
	arg += "set grid;"
	arg += "set title '" + title +"';"
	arg += "set xlabel '" + xl +"';"
	arg += "set ylabel '" + yl +"';"
	if graphs:
		arg+= 'plot "data" using 1:2 title "%s"' % (graphs[0])
		for i in range(len(graphs) - 1):
			arg+= ',"data" using 1:%d title "%s"' % (i + 3,graphs[i + 1])   #"data" using 1:3 lt 3 title "Remote"'			
	arg += ";set terminal wxt;"
	arg += "replot"
	prog = subprocess.Popen(['gnuplot', '--persist','-e',arg], shell = False)	
	
def _3d_plotter(points, graph = '', title = '', xl = '', yl = '', zl = ''):
	f = open('data', 'w')
	for a in points:
		for i in a:
			f.write(str(i) + ' ')
		f.write('\n\n')
	f.close()
	arg = ''
	arg += "set terminal postscript enhanced color solid size 20,20;"
	arg += "set output 'out.ps';"
#	arg += "set style data lines;"
	arg += "set pm3d;"
	arg += "set dgrid3d 50,50;"
	arg += "set hidden3d;"
	arg += "set ticslevel 0;"
	arg += "set isosample 40;"
	arg += "set title '" + title +"';"
	arg += "set xlabel '" + xl +"';"
	arg += "set ylabel '" + yl +"';"
	arg += "set zlabel '" + zl +"';"

	arg+= 'splot "data" u 1:2:3 with lines title "'+ graph +'";'

	arg += "set terminal wxt;"
	arg += "replot"
	prog = subprocess.Popen(['gnuplot', '--persist','-e',arg], shell = False)
	
def plot_bandwidth(time, data, limit = None, total = False, cache = False, remote = False):
	points = common.ordered_dict()
	first = float(rslt[0][10])
	count = 0
	fraction_saved = 0
	for a in rslt:
		count += 1
		if not limit == None:
			if count > limit:
				break
	#Timing
		arrival_time = float(a[10])
		dispatch_time = float(a[11])
		arrival_time -= first
		dispatch_time -= first
		mean_time = (dispatch_time + arrival_time)/2
		if not chk_x(mean_time): continue
	#Data
		bytes_requested_start = int(a[4])
		bytes_requested_end = int(a[5])
		#bytes_2_client = int(a[6]) # will always be same to one above
		bytes_cached = int(a[8]) - int(a[7])
		
		p_list = []
		
		if total:
			bytes_total = bytes_requested_end - bytes_requested_start
			p_list.append(bytes_total/data)
			
		if cache:
			bytes_from_cache = get_bytes_from_cache(bytes_requested_start,bytes_requested_end,bytes_cached)
			p_list.append(bytes_from_cache/data)
			
		fraction_saved += float(bytes_from_cache/bytes_total)
		if remote:
			bytes_from_remote = get_bytes_from_remote(bytes_requested_start,bytes_requested_end,bytes_cached)
			p_list.append(bytes_from_remote/data)
			

		
		try:
			value = points[int((mean_time)/time)]
			for i in range(len(p_list)):
				value[i] += p_list[i]
		except KeyError:
			points[int((mean_time)/time)] = p_list
	p = [[a,v] for a,v in points.items()]
	p.sort(key = lambda a: a[0])
	multiplotter(p,graphs = ['Total','Cache','Remote'], xl = 'Time (' + str(float(time/60.0)) + ' Minute(s)/Division)', yl = 'Data (kB/Division)', title = 'Bandwidth')
	print "Average Bandwidth saved: %s" % (fraction_saved*100/count)
	
	
def hit_ratio(time, data, limit = None):
	points = common.ordered_dict()
	first = float(rslt[0][10])
	count = 0
	for a in rslt:
		if not limit == None:
			count += 1
			if count > limit:
				break
	#Timing
		arrival_time = float(a[10])
		dispatch_time = float(a[11])
		arrival_time -= first
		dispatch_time -= first
		mean_time = (dispatch_time + arrival_time)/2
		if not chk_x(mean_time): continue
	#Data
		bytes_requested_start = int(a[4])
		bytes_requested_end = int(a[5])
		#bytes_2_client = int(a[6]) # will always be same to one above
		bytes_cached = int(a[8]) - int(a[7])
		
		
		bytes_total = bytes_requested_end - bytes_requested_start
			
		bytes_from_cache = get_bytes_from_cache(bytes_requested_start,bytes_requested_end,bytes_cached)

		try:
			value = points[int((mean_time)/time)]
			value[1] += 1
			value[0] += (bytes_from_cache/bytes_total)*(100.0)
		except KeyError:
			points[int((mean_time)/time)] = [float((bytes_from_cache/bytes_total)*(100.0)),1]
	p = [[a,[v[0]/v[1]]] for a,v in points.items()]
	p.sort(key = lambda a: a[0])
	multiplotter(p,graphs = ['Percentage hit'], xl = 'Time (' + str(float(time/60.0)) +' Minute(s)/Division)', yl = '% Hit (Percent/Division)', title = "Hit ratio")
	
	tmp = 0.0
	for a in p:
		tmp += a[1][0]
	tmp /= len(p)
	print "Average Hit ratio: %s" % (tmp) 

def wait_time(time, limit = None):
	points = common.ordered_dict()
	first = float(rslt[0][10])
	count = 0
	for a in rslt:
		if not limit == None:
			count += 1
			if count > limit:
				break
	#Timing
		arrival_time = float(a[10])
		dispatch_time = float(a[11])
		arrival_time -= first
		dispatch_time -= first
		mean_time = float(dispatch_time + arrival_time)/2
		avg_wait_time = float(dispatch_time - arrival_time) # in seconds
		if not chk_x(mean_time): continue
		
		try:
			value = points[int((mean_time)/time)]
			value[1] += 1
			value[0] += avg_wait_time
		except KeyError:
			points[int((mean_time)/time)] = [avg_wait_time,1]
			
	p = [[a,[v[0]/v[1]]] for a,v in points.items()]
	p.sort(key = lambda a: a[0])
	multiplotter(p,graphs = ['Wait time'], xl = 'Time (' + str(float(time/60.0)) + ' Minute(s)/Division)', yl = 'Average Waiting Time (Seconds/Division)', title = 'Wait time')
	
	total_wait = 0
	for a in p:
		total_wait += a[1][0]
	print "Average Waiting Time: %s" % (total_wait/len(p))
	

def datetime_2_timestamp(datetime):
	rslt = re.search(r'.*-(.*?) (.*?):(.*?):(.*)',datetime)
	return int(rslt.group(1)) * 24 * 60 * 60 + int(rslt.group(2)) * 60 * 60 + int(rslt.group(3)) * 60 + float(rslt.group(4))

def video_replacement_rate(time,data,limit = None):
	qry = "select * from videos_changed"
	rslt = cursor.execute(qry).fetchall()
	points = common.ordered_dict()
	first = datetime_2_timestamp(rslt[0][5])
	count = 0
	for a in rslt:
		if not limit == None:
			count += 1
			if count > limit:
				break
		i_range = common.text_2_byte_range(a[3])
		initial_prefix_length = i_range[1] - i_range[0]
		f_range = common.text_2_byte_range(a[4])
		final_prefix_length = f_range[1] - f_range[0]
		arrival_time = datetime_2_timestamp(a[5]) - first
		if final_prefix_length < initial_prefix_length:# currently final is always less in db
			bytes_replaced = initial_prefix_length - final_prefix_length
		else:
			bytes_replaced = 0
		try:
			points[int((arrival_time)/time)] += bytes_replaced/data
		except KeyError:
			points[int((arrival_time)/time)] = bytes_replaced/data
			
	p = [[a,[v]] for a,v in points.items()]
	p.sort(key = lambda a: a[0])
	multiplotter(p,graphs = ['Bytes replaced'], xl = 'Time (' + str(float(time/60.0)) + ' Minute(s)/Division)', yl = 'Cache data replaced (kB/Division)', title = 'Cache replacement rate')
	
if __name__ == "__main__":
	#plot_total(1,1024,'sec','kb', 10000)f.write(str(i) + ' ')
	#plot_remote_bandwidth(1,1024,'sec','kb', 10000) # kb/sec
	#plot_cache_bandwidth(1,1024,'sec','kb') # kb/sec
	ch = sys.argv[1]
	
	if ch == '1':
		plot_bandwidth(60*2,1024,total = True, cache = True, remote = True)
	elif ch == '2':
		hit_ratio(60*2,1024)
	elif ch == '3':
		#wait_time(60*1,1024)
		wait_time(60*2)
	elif ch == '4':	
		video_replacement_rate(60*2,1024)
		
	
	print "Total error: %d" % ERROR
	conn.close()

