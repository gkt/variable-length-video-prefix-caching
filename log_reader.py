#!usr/bin/python

#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.

import re
import sys
import os

import config

LOG_FILE = os.path.expanduser('~') + "/" + config.PROG_DIR_NAME + "/" + config.LOG_NAME

if __name__ == "__main__":

	ptrn = sys.argv[1]
	fp = open(LOG_FILE, "r")
	buf = fp.read()
	#itr = re.finditer("#@([^'#@']*?" + ptrn + "[^'=@']*?)=@", buf, re.DOTALL| re.MULTILINE)
	slog = buf.split('==')
	for a in slog:
		if re.search(ptrn + '[^0-9]',a):
			print a
