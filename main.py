#!/usr/bin/env python

#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.


__version__ = '0.3'



import BaseHTTPServer
import select
import socket
import SocketServer
import urlparse
import re
import os
import sys
import time
import getopt
import datetime
import logging
import logging.handlers
import threading
from email.utils import formatdate

import db
import common
import config

CACHE_SWITCH = True

#create program's root folder
RDIR = os.path.expanduser('~') + "/" + config.PROG_DIR_NAME
if not os.path.exists(RDIR):
	print "Creating " + RDIR
	os.makedirs(RDIR)
	
#logger
LOG_FILE = os.path.expanduser('~') + "/" + config.PROG_DIR_NAME + "/" + config.LOG_NAME
_logger = logging.getLogger('main')
_logger.setLevel(logging.DEBUG)

fh = logging.handlers.RotatingFileHandler(LOG_FILE,maxBytes=1024*1024*10,backupCount=1,)
fh.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter("""==[%(asctime)-15s] %(levelname)s %(name)s (%(threadName)-10s): %(message)s""")
ch.setFormatter(formatter)
fh.setFormatter(formatter)

_logger.addHandler(ch)
_logger.addHandler(fh)

#globals
dbase = db.db()

#Lock for access to dbase variable
DB_LOCK = threading.Condition(threading.Lock())

class ProxyHandler (BaseHTTPServer.BaseHTTPRequestHandler):
	"""This class handles the HTTP requests arriving at proxy."""
	__base = BaseHTTPServer.BaseHTTPRequestHandler

	server_version = "VideoCache " + __version__
	rbufsize = 0                        # self.rfile Be unbuffered
	

	def _connect_to(self, netloc, soc):
		i = netloc.find(':')
		if i >= 0:
			host_port = netloc[:i], int(netloc[i+1:])
		else:
			host_port = netloc, 80
		_logger.info("Connecting to server at %s:%d" % host_port)
		try: soc.connect(host_port)
		except socket.error, arg:
			try: msg = arg[1]
			except: msg = arg
			self.send_error(404, msg)
			return 0
		return 1

	def do_CONNECT(self): # supports only custom connect request from test.py
		soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		self.path = self.headers['Mypath']
		self.command = 'GET'
		self.request_version = 'HTTP/1.1'
		

		del self.headers['Mypath']
		
		#call do_GET()
		self.do_GET()


	def do_GET(self):
		"""handles HTTP GET request"""
		self.cl_add = str(self.client_address[0]) + ':' + str(self.client_address[1])
		_logger.info("Client: %s" % self.cl_add)
		(scm, netloc, path, params, query, fragment) = urlparse.urlparse(
			self.path, 'http')
			
		self.GET_request = "%s %s %s\r\n" % (
					self.command,
					urlparse.urlunparse(('', '', path, params, query, '')),
					self.request_version)

		#create and connect socket to remote server
		soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self._connect_to(netloc, soc)
		#self.log_request()
		
		#check if it's a video url
		if dbase.is_vid(self.path) and CACHE_SWITCH:
			self._redirect_to_cache(soc)
			return
		
		try:
			soc.send(str(self.GET_request))
						
			self.headers['Connection'] = 'close'
			del self.headers['Proxy-Connection']
			soc.send(common.join_headers(self.headers))
			fetched_bytes = self._read_write(soc)# no use of returned data here
			if dbase.is_vid(self.path):
				_logger.info("Received %d bytes form server" % len(fetched_bytes))
			
		finally:
					
			_logger.info("Closing connections")
			soc.close()
			self.connection.close()
			
##Client on self.connection
##Server on soc

	def _redirect_to_cache(self,soc):
		global DB_LOCK
		_logger.info( "From client:\n" + self._debug_format(self.GET_request + common.join_headers(self.headers)))
				
		try:
			range_header = self.headers['Range']
		except KeyError:#if "range:" header does not exist then insert one
			range_header = self.headers['Range'] = 'bytes=0-'
			#print "KILLED"
			#return #TESTING ONLY		
		
		rng = self._text_2_byte_range(range_header)
		
		
		total_bytes_send = '' # every byte that is send to client
		header_dict = None
		total_vid_size = None
		is_cached = False
		is_retrieved = False
		delayed_hd_dt = ''
		
		#initialize stats list
		stats = range(11)
		stats[9] = time.time() # request arrival time stamp
		stats[1] = datetime.datetime.now()
		DB_LOCK.acquire()
		try:
			# get cache video prefix, if available
			process_list,c_bytes,c_header,c_total_vid_size = dbase.get_cache(self.path,rng)
		finally:
			DB_LOCK.release()
		
		try:
			for a in process_list:
				if a[2] == True: # send bytes received from cache
					is_cached = True
				
					c_header_dict = common.parse_headers(c_header)
		
					if rng[1] == config.MAX_LEN:# full video requested
						l1 = "HTTP/1.1 200 OK\r\n"
						c_header_dict["Content-Length"] = c_total_vid_size
					else:
						l1 = "HTTP/1.1 206 Partial Content\r\n"
						c_header_dict["Content-Length"] = str(rng[1] - rng[0] + 1)
						content_range = "bytes %d-%d/%s" % (rng[0],rng[1],c_total_vid_size)
						
						
						#to maintain order remove "Connection" and "Content-Type", then re-insert them after "Content-Range"
						del c_header_dict["Connection"]
						content_type = c_header_dict["Content-Type"]
						del c_header_dict["Content-Type"]
						c_header_dict["Content-Range"] = content_range
						c_header_dict["Connection"] = "close"
						c_header_dict["Content-Type"] = content_type
				
					c_header_dict["Date"] = formatdate(timeval=None, localtime=False, usegmt=True)
			
					processed_c_headers = str(l1 + common.join_headers(c_header_dict))
					
					if rng[1] - rng[0] + 1 > config.MIN_BYTE_2_CLIENT and len(c_bytes) < config.MIN_BYTE_2_CLIENT:
						_logger.info("Delaying response to client,wait for more data to arrive")
						#_logger.info( "To client:(processed)\n%s\n\nData: %d" % (self._debug_format(processed_c_headers), len(c_bytes)))
						delayed_hd_dt = processed_c_headers + c_bytes
					else:
						_logger.info( "To client:(processed)\n%s\n\nData: %d" % (self._debug_format(processed_c_headers), len(c_bytes)))
						self.connection.send(processed_c_headers + c_bytes) # unicode + binary(in string) leads to error
						stats[10] = time.time() # dispatch time stamp
						
					total_bytes_send += c_bytes
					
				else:# else get data from remote server
					is_retrieved = True
					soc.send(self.GET_request)				
					self.headers['Range'] = 'bytes=' + str(a[0]) + '-' + str(a[1])
					self.headers['Connection'] = 'close'
					del self.headers['Proxy-Connection']
					soc.send(common.join_headers(self.headers))
					
					_logger.info("To remote server:\n" + self._debug_format(self.GET_request + common.join_headers(self.headers)))
					
					ret = self._read_write_cache(soc,is_cached,delayed_hd_dt)
					stats[10] = time.time() # dispatch time stamp
					
					header_dict = ret[0]
					
					if header_dict: # if client didn't disconnect prematurely, then parse result
						
						#extract total video size
						if re.compile("206 Partial Content").search(ret[1]):
							rslt = re.compile(".*/(.*)").search(header_dict["Content-Range"])
							total_vid_size = rslt.group(1)
						else:
							total_vid_size = header_dict["Content-Length"]
						
						#update total bytes
						if ret[2]:
							total_bytes_send += ret[2]
		finally:
			DB_LOCK.acquire()
			try:
				if not is_retrieved: # 100% from cache
					_logger.info("100% HIT")
					dbase.update_cache(rng,self.path,total_bytes_send,c_header,c_total_vid_size,self.cl_add,stats)
				else:
					if not header_dict:# tried to retrieve data from remote server, but got no headers (case of early connection termination)
						_logger.warning("Client terminated connection, Skipping any update to db")
					else:
						_logger.info("Sending update request to cache with %s bytes" % len(total_bytes_send))
						dbase.update_cache(rng,self.path,total_bytes_send,common.join_headers(header_dict),total_vid_size,self.cl_add,stats)
			finally:
				DB_LOCK.release()
			_logger.info("Closing connections")
			soc.close()
			self.connection.close()

	def _text_2_byte_range(self,txt):
		rslt = re.compile("bytes=(.*)-(.*)").search(txt)
		a = int(rslt.group(1).strip())
		b = rslt.group(2).strip()
		if not b:
			b = config.MAX_LEN#as large as possible
		else:
			b = int(b)
		return [a,b]
		
	def _join_headers(self,d):
		hd = ''
		for key_val in d.items():
			hd += "%s: %s\r\n" % key_val
		hd += "\r\n"
		return hd
		
	def _parse_headers(self,header):
		od = ordered_dict()
		for a in re.findall(r"(?P<name>.*?): (?P<value>.*?)\r\n", header):
			od[a[0]] = a[1]
		return od
		
	def _read_write(self, soc, max_idling=20):
		"""create a pipe between remote server and client"""
		iw = [self.connection, soc]
		ow = []
		count = 0
		fetched_bytes = ''

		while 1:
			count += 1
			(ins, _, exs) = select.select(iw, ow, iw, 3)
			if exs: break
			if ins:
				for i in ins:
					if i is soc:
						out = self.connection
					else:
						out = soc
					data = i.recv(8192)
					if data:
						if i is soc:
							fetched_bytes += data
						out.send(data)
						count = 0
			else:
				_logger.info("Idle %s" % count)
			if count == max_idling: break
		
		return fetched_bytes
		
	def _read_write_cache(self, soc, is_cached,delayed_hd_dt,max_idling=20):
		"""create two pipes one between cache and remote server, and other between cache and client"""
		iw = [self.connection, soc]
		ow = []
		count = 0
		fetched_bytes = ''
		fetched_headers = ''
		GET_reply = ''

		flag = True
		try:
			while 1:
				count += 1
				(ins, _, exs) = select.select(iw, ow, iw, 3)
				if exs:
					print "'select' exception"
					break
				if ins:
					for i in ins:
						if i is soc:
							out = self.connection
						else:
							out = soc
						data = i.recv(8192)
						if data:
							count = 0
							if i is soc:
							
								if flag:
									rslt = re.compile("\r\n\r\n").search(data)
									headers = data[:rslt.start() + 2]
									fetched_bytes = data[rslt.end():]
								
									GET_reply = re.compile("(HTTP.*\r\n)").search(headers).group(1)								
									fetched_headers = common.parse_headers(headers)															
								
								
									_logger.info("From remote server:\n" + self._debug_format(headers) + "\n#END\n")
								
									if is_cached:
										if delayed_hd_dt:
											rsl = re.compile("\r\n\r\n").search(delayed_hd_dt)
											hd = delayed_hd_dt[:rslt.start() + 2]
											f_b = delayed_hd_dt[rslt.end():]
											_logger.info("To client:\nDelayed header:\n%s\nDelayed data: %d bytes\n" % (hd,len(f_b)) + "\n#END\n")

											#out.send(headers + f_b + fetched_bytes)
											out.send(delayed_hd_dt + fetched_bytes)
										else:
											#_logger.info("To client:\nData: %d bytes" % len(fetched_bytes)  + "\n#END\n")
											out.send(fetched_bytes)
									else:
										_logger.info("To client:\n%s\n\n" % (self._debug_format(headers))  + "\n#END\n")
										out.send(data)
								
									flag = False
								else:
									#_logger.info("To client:\nData of length: %d" % len(data)  + "\n#END\n")
									out.send(data)
									fetched_bytes += data
							else:
								out.send(data)
						
				else:
					_logger.info("Idle %s" % count)
				if count == max_idling:
					_logger.warning("Connection timed-out")
					break
					
		except Exception as e:
			_logger.warning("Client terminated connection")
		
		if fetched_bytes:
			_logger.info("To client:\nData(Total): %d bytes" % len(fetched_bytes)  + "\n#END\n")
		#sometimes it will return empty values if connection closed early,i.e ['','',''] 
		return [fetched_headers,GET_reply,fetched_bytes]

	def _debug_format(self,txt):
		"""replace \n \t characters in headers for output to terminal"""
		b = ''
		flag = False
		for a in txt:
			if a == '\n':
				b += '\\n'
				flag = True
			elif a == '\r':
				b += '\\r'
				flag = True
			else:
				if flag:
					b += '\n'
					flag = False
				
				b += a
				
		return b
			
		
	do_HEAD = do_GET
	do_POST = do_GET
	do_PUT  = do_GET
	do_DELETE=do_GET
	
#allow_reuse_address = 1; in HTTPServer class 
class Modified_HTTPServer(BaseHTTPServer.HTTPServer):
	"""for overriding HTTPServer class variables"""
	#allow_reuse_address = True
	request_queue_size = 10
	
class ThreadingHTTPServer (SocketServer.ThreadingMixIn,
						   Modified_HTTPServer):
	pass
						   
def start_proxy(HandlerClass,ServerClass, protocol="HTTP/1.0"): # test with 1.1
	"""start proxy server on specified port"""
	global CACHE_SWITCH
	port = 8000
	try:
		optlist, args = getopt.getopt(sys.argv[1:], 'op:')
	except getopt.GetoptError, err:
		usage(sys.argv[0])
		
	if args:
		usage(sys.argv[0])
	for o,a in optlist:
		if o == '-p':
			port = a
		elif o == '-o':
			CACHE_SWITCH = False
		else:
			usage(sys.argv[0])
			

	server_address = ('127.0.0.1', port)

	HandlerClass.protocol_version = protocol
	httpd = ServerClass(server_address, HandlerClass)

	sa = httpd.socket.getsockname()
	_logger.info("Serving HTTP on %s port %d ..." % ( sa[0], sa[1]))
	if not CACHE_SWITCH:
		_logger.info("CACHE DISABLED")
	httpd.serve_forever()
						   
def usage(prog):
	print "%s usage\n-o:\tDisable cache(optional)\n-p <port>:\tPort(Optional default is 8000)" % prog
	sys.exit(0)
 
if __name__ == '__main__':
	start_proxy(ProxyHandler, ThreadingHTTPServer)
