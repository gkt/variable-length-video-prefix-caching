
#Copyright 2011-2012 Gaurav Kumar, Gaurav Aggarwal

#This file is part of Video Prefix Cache Proxy (VPC_Proxy).

#Video Prefix Cache Proxy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Video Prefix Cache Proxy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Video Prefix Cache Proxy.  If not, see <http://www.gnu.org/licenses/>.

import logging
import datetime
import os
import sqlite3
import threading

from common import *

class popularity():
	def __init__(self,conn):
		self.hit_threshold = 2 #minimum 2 hits required
		self.date_offset = 90 #days
		self.logger = logging.getLogger('main.db.popularity')
		self.conn = conn
		self.max_popularity = self.get_max_popularity()[0][0]
		self.last_run_id = self.get_last_run_id() + 1
		self.set_all_info(0)	
		self.lock = threading.Condition(threading.Lock())

	def get_last_run_id(self):
		expected_id = self.extract("select max(ID) from more_space_runs")[0][0]
		if expected_id == None:
			expected_id = 1
		else:
			expected_id = expected_id + 1
		return expected_id 
		
	def set_all_info(self,expected_id): #  0 expected_id means initial run from constructor
	
		self.logger.info("Preprocessing all previous requests and updating cache")
		dump = self.extract("select url_hash,byte_range,last_popularity,total_size,file_loc from cache_info")	
		for x in dump:
			cur_popularity = self.get_popularity(x[0]) 
			self.update_last_popularity(x[0],cur_popularity)
						
		# max_popularity is now using new global maximum value
		dump = self.extract("select url_hash,byte_range,last_popularity,total_size,file_loc from cache_info")	
		for x in dump:
			start_time = datetime.datetime.now()

			# add a delete filehandler if size is zero
			new_byte_range = self.get_prefix_length(x[2],x[3]) # 0 till this
			c_rng = text_2_byte_range(x[1])
			if new_byte_range < (c_rng[1] - c_rng[0] + 1):# if cache needs to be reduced
				if new_byte_range == 0:# cache_info and cached data entry needs to be removed
					os.remove(x[4])
					self.logger.info("Removing %s's cache data" % x[0])
					self.update_byte_range(x[0],"0-0")
					self.insert_video_in_run([expected_id,x[0],x[1],"0-0",start_time,datetime.datetime.now()])
				else:# truncate cached data and update cache_info
					fp = open(x[4],"r+b")
					fp.seek(new_byte_range)
					fp.truncate()
					fp.close()
					self.logger.info("Truncating %s's cache data to %d bytes" % (x[0],new_byte_range))
					new_range = "0-"+str(new_byte_range - 1)
					self.insert_video_in_run([expected_id,x[0],x[1],new_range,start_time,datetime.datetime.now()])
					self.update_byte_range(x[0],new_range)
					#print new_byte_range,'######'
			else:# cache needs to be extened with new data
				pass# possible case fetch in advance more data form remote server
				
	def insert_runs(self,lst):
		self.conn.execute("""insert into more_space_runs (id,start_time,end_time,succeeded,hit_count_start,hit_count_end) values (?,?,?,?,?,?)""",lst)
		
	def insert_video_in_run(self,lst):
		return self.conn.execute("""insert into videos_changed (run_id,url_hash,previous_byte_range,new_byte_range,start_time,end_time) values (?,?,?,?,?,?)""",lst)
	
	def create_more_space(self):
		possible = 0
		start_time = datetime.datetime.now()
		hit_count_start = self.hit_threshold
		ref_date = start_time - datetime.timedelta(days= self.date_offset) 
		
		# get expected id
		self.lock.acquire()
		try:
			expected_id = self.last_run_id
			self.last_run_id = self.last_run_id + 1
		finally:
			self.lock.release()
			
		dump = self.extract("select  count(*) from request group by url_hash having date > '"+str(ref_date)+"' order by count(*)")
		for x in dump:
			if x[0] > self.hit_threshold:
				self.hit_threshold = x[0]
				possible = 1	
				self.set_all_info(expected_id)
				break
		
		#make entry in replacement logs
		self.insert_runs([expected_id,start_time,datetime.datetime.now(),possible,hit_count_start,self.hit_threshold])
		
		if possible == 1: 
			self.logger.info("Increased Hit Threashold to "+ str(self.hit_threshold) +" and created more space ")	
			return 1
		else:
			self.logger.info("Impossible to increase Hit Threshold : Unable to create more space");
			return 0

	def get_max_popularity(self):
		self.logger.info("Querying for maximum popularity as reference")
		return self.extract("select max(last_popularity) from cache_info")
		
	def update_last_popularity(self,url_hash,popularity):
		# assuming an entry already exists in cace_info table
		return self.conn.execute("update cache_info set last_popularity = "+str(popularity)+" where url_hash='"+str(url_hash) + "'")

	def update_byte_range(self,url_hash,byte_range):
		if byte_range == '0-0':# remove entry
			return self.conn.execute("delete from cache_info where url_hash='"+str(url_hash)+"'")
		else:# update entry
			#print "update cache_info set byte_range = "+str(byte_range)+" where url_hash='"+str(url_hash)+"'"
			return self.conn.execute("update cache_info set byte_range = '"+str(byte_range)+"' where url_hash='"+str(url_hash)+"'")

	def make_request_entry(self,client_ip,url_hash,byte_range,bytes_len):
		byte_range_used =  str(byte_range[0]) + "-" + str(bytes_len + byte_range[0] - 1)
		return self.conn.execute("insert into request (client_ip, date, url_hash, byte_range_used) values('"+str(client_ip)+"',datetime(),'"+url_hash+"','"+byte_range_used+"')")

	def get_popularity(self,url_hash):
		self.logger.info(" popularity for url_hash required "+url_hash)
		cur_popularity = 0.0
		hit_count = 0
		ref_date = datetime.datetime.now() - datetime.timedelta(days= self.date_offset) 
		dump = self.extract("select  count(*),DATE(date) from request group by DATE(date),url_hash having url_hash = '"+url_hash+"' AND date> '"+str(ref_date)+"'")	
		for x in dump:
			hit_count = hit_count + x[0]
			cur_popularity = cur_popularity + x[0] * (datetime.datetime.strptime(x[1], '%Y-%m-%d') - ref_date).days

		self.logger.info(" video count "+str(hit_count))
		if hit_count >= self.hit_threshold:
			cur_popularity = cur_popularity/len(dump)
			return cur_popularity

		return 0.0 

	def popularity_check(self,client_ip,url_hash,byte_range,bytes,total_vid_size):# will return byte size of cache from starting
		self.logger.info("Popularity check of "+url_hash+" on request from "+client_ip)
		self.logger.info(" Current hit count is " + str(self.hit_threshold))
		bytes_len = len(bytes)
		self.make_request_entry(client_ip,url_hash,byte_range,bytes_len)
		val = self.get_popularity_based_prefix_length(url_hash,total_vid_size)
		#self.logger.info(" value returned from length calculator "+str(val))
		return val
		
	def get_popularity_based_prefix_length(self,url_hash,total_vid_size):
		bytes_len = int(total_vid_size)
		cur_popularity = self.get_popularity(url_hash) 
		self.logger.info(" calculated popularity "+str(cur_popularity))
		#self.update_last_popularity(url_hash,cur_popularity)
		return [self.get_prefix_length(cur_popularity,bytes_len) , cur_popularity ]
	
	def get_prefix_length(self,cur_popularity,bytes_len):		
	#       TODO Discuss doubts about using bytes length vs total_video length for % prefix calculation
		if cur_popularity < 0.1:
			return 0

		self.logger.info("maximum popularity "+str(self.max_popularity))
		if cur_popularity > self.max_popularity:
			self.max_popularity = cur_popularity
			return bytes_len
			
		self.logger.info("popularity by formula ")
		return   int(float(cur_popularity/self.max_popularity)*float(bytes_len))
		
	def extract(self,query):
		cursor = self.conn.cursor()
		return cursor.execute(query).fetchall()

			
#if __name__ == "__main__":
	#homedir = os.path.expanduser('~')
	#PROG_DIR_NAME = 'pytrace_proxy'
	#progdir = homedir + "/" + PROG_DIR_NAME
	#db_file = progdir + '/table.db'
	#file_exists = os.path.exists(db_file)
	#conn = sqlite3.connect(db_file,check_same_thread = False,isolation_level = None) # autocommit mode with threading
	#a = popularity(conn)
	#a.create_more_space()
